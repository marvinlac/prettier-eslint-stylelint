# Prettier, ESLint, stylelint example 🚀

Fully working example project with Prettier, ESLint and stylelint - nothing else!

This example is made for Visual Studio Code, Sass and React, but everything can be adjusted to your liking of course!

### Features

- Lint warnings and errors are directly shown in the editor
- Format on save
- ESLint on save
  - Airbnb config
  - React configs
- stylelint on save
  - Sass config
  - Orders CSS Properties automatically
- Format and lint as pre-commit hook (With husky and lint-staged)
  - Cancels commit on linting warnings/errors

### Try it out

```
npm i
```

- Install extentions for Prettier, ESLint and stylelint
- Make changes to any of the files and save it
  - Lint errors should be shown and file should get formatted
- Deactivate VS Code settings (codeActionsOnSave -> source.fixAll and formatOnSave) to test the pre-commit hook

# How to integrate into your project 🔨

All files and their contents inside this repository are relevant, except for src/\*.

The easiest way is to use everything as-is, but you can definitely use other Prettier/ESLint/stylelint configs for example!

The high-level steps look like this:

- Package.json:
  - Install dependencies seen in package.json
    - Do not just copy-paste, but npm/yarn install them, e.g. _npm i -D -E eslint_
  - Copy the husky and lint-staged objects to your package.json
  - Optional: Copy scripts if you want to manually trigger the actions
- Copy all config and \*ignore files from the root folder to your project
- Copy/Merge settings from the .vscode folder
- Install Extentions for Prettier, ESLint and stylelint

# FYI: Integration of ESLint and stylelint with Prettier ❕

This example uses **eslint-config-prettier** to disable any rules that conflict with Prettier, e.g. ESLint wants _singleQuote: true_ and Prettier _singleQuote: false_ (See the **prettier** entry in the extends array of the eslint config.)

There is also an option to run Prettier from ESLint, which is **not** done by this example. Prettier is run separatly to keep things - eeehm - separate ;). This way we can run Prettier once for all files, when commiting.

The same applies to stylelint with **stylelint-config-prettier**.

See [Prettier - Integrating with Linters](https://prettier.io/docs/en/integrating-with-linters.html) for more information.
